import axios from 'axios'

export default {
  getNextBuses(stationId) {
    return axios
      .get(`/api/yelo-api/-/data/bus/siri/next/${stationId}/10.json`)
      .then(response => {
        return response.data.nextBuses
      })
  }
}
