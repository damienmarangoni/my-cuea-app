import Vue from 'vue'
import Vuex from 'vuex'

import * as transport from '@/store/modules/transport'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    transport
  }
})
